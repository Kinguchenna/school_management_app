﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;
using System.Data;
using System.IO;
using System.Drawing.Imaging;
using MySql.Data.MySqlClient;
namespace WindowsFormsApplication10
{
    class database
    {
        public string conn;
        public MySqlConnection cnn;
        public MySqlCommand cmd = new MySqlCommand();
        public Biodata myData = new Biodata();
        public MySqlDataAdapter myAdapter = new MySqlDataAdapter();
        public DataTable dTable = new DataTable();
        public login myLog = new login();
        public OpenFileDialog openDia = new OpenFileDialog();
        public string query;
        public Form1 myfrm = new Form1();
     

        public database() {
           conn = "server=localhost;uid=root;pwd=pass123;database=cms";
           cnn = new MySqlConnection(conn);
        }

        public void openConnection() {
            cnn.Open();
        }
        public void closeConnection() {
            cnn.Close();
        }
        public void chk() {
            CheckBox myCHCK1 = Application.OpenForms["login"].Controls["checkBox1"] as CheckBox;
            CheckBox myCHCK2 = Application.OpenForms["login"].Controls["checkBox2"] as CheckBox;
            if (myCHCK1.Checked)
            {
                myCHCK2.Hide();

            }
            else if (!myCHCK1.Checked)
            {
                myCHCK2.Show();
            }

        }
        public void chk1()
        {
            CheckBox myCHCK1 = Application.OpenForms["login"].Controls["checkBox1"] as CheckBox;
            CheckBox myCHCK2 = Application.OpenForms["login"].Controls["checkBox2"] as CheckBox;
            if (myCHCK2.Checked)
            {
                myCHCK1.Hide();

            }
            else if (!myCHCK2.Checked)
            {
                myCHCK1.Show();
            }

        }
        public void upLoad() {               
            openDia.ShowDialog();
            PictureBox myPic = Application.OpenForms["Form1"].Controls["pictureBox1"] as PictureBox;
            string fileName = openDia.FileName;
                myPic.Image = Image.FromFile(fileName);
                
            }

        public void AdminLog(string email, string pass) {
            openConnection();
            query = "select * from admin where email = '" + email + "' and password =  '" + pass + "'";
            cmd = new MySqlCommand(query,cnn);
            myAdapter.SelectCommand = cmd;
            myAdapter.Fill(dTable);
            if (dTable.Rows.Count > 0)
            {
                cmd.ExecuteNonQuery();
                MessageBox.Show("Login Granted");
                myfrm.Show();
                myLog.Hide();
                closeConnection();
            }
            else if (string.IsNullOrWhiteSpace(email) || string.IsNullOrWhiteSpace(pass))
            {
                MessageBox.Show("Please all filed are Required");
            } else { MessageBox.Show("Login Incorrect"); };
    }
        
        public void LoGin(string ID, string password) {                  
                openConnection();
                query = "select * from user1 where student_id = '" + ID + "' and password =  '"+password+"'";
                cmd = new MySqlCommand(query, cnn);
                myAdapter.SelectCommand = cmd;
                myAdapter.Fill(dTable);
            if (dTable.Rows.Count > 0)
            {
                cmd.ExecuteNonQuery();
                MessageBox.Show("Login Successfully");
                
                myData.Show();
                
                myLog.Hide();
                closeConnection();
            }            
            else if (string.IsNullOrWhiteSpace(ID) || string.IsNullOrWhiteSpace(password)) {
                MessageBox.Show("Login Information Required", "Contact Admin for Login Details");
                closeConnection();

            }
            else { MessageBox.Show("Login Details is Incorrect", "Confirm LogIn"); }
            closeConnection();                           
             }
       
        public void delete( string text) {

            openConnection();
            string query = "delete from user1 where id = '" + text + "';";
             cnn = new MySqlConnection(conn);
             cmd = new MySqlCommand(query, cnn);
            cmd.CommandTimeout = 60;
            var Result = MessageBox.Show("Do You Still Want To Delete", "Yes or No",MessageBoxButtons.YesNo,MessageBoxIcon.Information);
            if (Result == DialogResult.Yes)
            {
                try
                {
                    cnn.Open();
                    MySqlDataReader myReader = cmd.ExecuteReader();
                    MessageBox.Show("Record Deleted Successfully");
                    cnn.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                
            }
            else { MessageBox.Show("Nothing Was Deleted"); }
            


        }
        public void Update() {
        
}

        public void BioData() {
            TextBox myText = Application.OpenForms["login"].Controls["textbox1"] as TextBox;
          
            string query = "select * from user1 where student_id ='"+myText.Text+"'";
            cnn = new MySqlConnection(conn);
            cmd = new MySqlCommand(query, cnn);
            openConnection();
            Label label1 = Application.OpenForms["Biodata"].Controls["label18"] as Label;
            Label label2 = Application.OpenForms["Biodata"].Controls["label17"] as Label;
            Label label3 = Application.OpenForms["Biodata"].Controls["label2"] as Label;
            Label label4 = Application.OpenForms["Biodata"].Controls["label4"] as Label;
            Label label5 = Application.OpenForms["Biodata"].Controls["label3"] as Label;
            Label label6 = Application.OpenForms["Biodata"].Controls["label8"] as Label;
            Label label7 = Application.OpenForms["Biodata"].Controls["label7"] as Label;
            Label label8 = Application.OpenForms["Biodata"].Controls["label19"] as Label;
            Label label9 = Application.OpenForms["Biodata"].Controls["label5"] as Label;
            Label label10 = Application.OpenForms["Biodata"].Controls["label6"] as Label;
            Label label11 = Application.OpenForms["Biodata"].Controls["label29"] as Label;
            Label label12 = Application.OpenForms["Biodata"].Controls["label15"] as Label;
            PictureBox Picdata = Application.OpenForms["Biodata"].Controls["pictureBox1"] as PictureBox;
            MySqlDataReader myReader = cmd.ExecuteReader();
            while (myReader.Read()) { label1.Text = myReader[1].ToString(); label2.Text = myReader[2].ToString(); label3.Text = myReader[3].ToString();
                label4.Text = myReader[4].ToString(); label5.Text = myReader[5].ToString();
                label6.Text = myReader[8].ToString(); label7.Text = myReader[9].ToString();
                label8.Text = myReader[10].ToString(); label9.Text = myReader[11].ToString();
                label10.Text = myReader[12].ToString(); label11.Text = myReader[6].ToString();
                label12.Text = myReader[13].ToString();
                byte[] picT = myReader["pic"] as byte[] ?? null;
                MemoryStream ms = new MemoryStream(picT);
                Picdata.Image = System.Drawing.Image.FromStream(ms);
                
                System.Drawing.Bitmap Bmp = new System.Drawing.Bitmap(ms);
               
            }
            closeConnection();
        }
        public void inSert(string student, string firstname, string lastname, string address, string address1, string phone, string phone1, string state, DateTime dob, string g_phone, string g_address, string g_name, string more_info, Image myPic) {

            try
            {
                openConnection();
                
                
               query = "insert into user1 values (null,@student,@firstname,@lastname,@address,@address1,@phone,@phone1,@state,@dob,@g_phone,@g_address,@g_name,@more_info,@myPic)";
                cmd = new MySqlCommand(query, cnn);
                cmd.Parameters.AddWithValue("@student", student);
                cmd.Parameters.AddWithValue("@firstname", firstname);
                cmd.Parameters.AddWithValue("@lastname", lastname);
                cmd.Parameters.AddWithValue("@address", address);
                cmd.Parameters.AddWithValue("@address1", address1);
                cmd.Parameters.AddWithValue("@phone", phone);
                cmd.Parameters.AddWithValue("@phone1", phone1);
                cmd.Parameters.AddWithValue("@state", state);
                cmd.Parameters.AddWithValue("@dob", dob);
                cmd.Parameters.AddWithValue("@g_phone", g_phone);
                cmd.Parameters.AddWithValue("@g_address", g_address);
                cmd.Parameters.AddWithValue("@g_name", g_name);
                cmd.Parameters.AddWithValue("@more_info", more_info);
                
                MemoryStream ms = new MemoryStream();
                myPic.Save(ms, ImageFormat.Jpeg);
                byte[] pic_arr = new byte[ms.Length];
                ms.Position = 0;
                ms.Read(pic_arr,0,pic_arr.Length);
                cmd.Parameters.AddWithValue("@myPic",pic_arr );
                cmd.ExecuteNonQuery();
                MessageBox.Show("Record Inserted Successfully");
                closeConnection();
            } catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

    }
}
