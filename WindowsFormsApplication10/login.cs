﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
namespace WindowsFormsApplication10
{
    public partial class login : Form
    {
        public static string passingText;

        public login()
        {
            InitializeComponent();
        }
        private void button1_Click(object sender, EventArgs e)
        {
            if (checkBox1.Checked)
            {
                string pass = textBox2.Text;
                string email = textBox1.Text;
                database myLogin = new database();
                myLogin.LoGin(email, pass);
            }
            else if (checkBox2.Checked)
            {
                string pass = textBox2.Text;
                string email = textBox1.Text;
                database admin = new database();
                admin.AdminLog(email,pass);
            }
            else { MessageBox.Show("Nothing was checked","Login as Admin or User"); };
        }

        private void login_Load(object sender, EventArgs e)
        { }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Hide();
            Registration myReg = new Registration();
            myReg.Show();
        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        { }
        private void checkBox1_Click(object sender, EventArgs e)
        {
            database mycheck = new database();
            mycheck.chk();
        }

        private void checkBox2_Click(object sender, EventArgs e)
        {
            database mycheck = new database();
            mycheck.chk1();
        }
    }
}